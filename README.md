# Modbus Broker
In a Modbus TCP/IP system there is a Modbus master and one or more Modbus slaves. The Modbus master is the one making the requests to read or write values from the Modbus slave and the Modbus slave only listens for requests from the server and responds accordingly (in other words the Modbus slave will only listen for requests and will not send any request to read or write data).

Usually there is a Modbus master client and a Modbus slave server, where the Modbus master connects to the Modbus slave server.
This creates the problem where you have one Modbus master that wants to read/write data from multiple Modbus slaves as the Modbus master
can only connect to a single server this is not possible. This is where the Modbus broker comes in.

The Modbus broker application facilitates the communication between the Modbus master and the Modbus slave(s). 
Which makes it possible for a Modbus master to communicate with multiple Modbus slaves.

Below is a picture showing how the Modbus master and Modbus slave connects to the Modbus broker application.
![Modbus Connections](https://i.imgur.com/yIiFDYe.png)

Below is a picture showing how the request/response is flowing from the Modbus master to the Modbus slave (through the Modbus broker).
![Modbus Connections](https://i.imgur.com/14fYkxM.png)

Examples of Modbus master clients are: [Vellamod](http://www.tuomio.fi/vellamod/index.htm), [Mach3](http://www.machsupport.com/)

Examples of Modbus slave clients are: [Modbus Slave Example](https://gitlab.com/modbus/modbus-slave-example)

## Run
To start the Modbus broker application, run the following command:
```
git clone https://gitlab.com/modbus/modbus-broker
cd modbus-broker
gradle runApplication
```

## Build
To create a jar file run the following command:
```
gradle fatJar
```

The jar file can then be found at "build/libs/modbus-broker-all-1.0-SNAPSHOT.jar".
To run it, execute the following command:
```
java -jar modbus-broker-all-1.0-SNAPSHOT.jar
```

