package com.gitlab.soshibby.modbus.broker.events;

import com.gitlab.soshibby.modbus.broker.client.ModBusClient;

public interface ConnectionEventListener {
    void onConnected(ModBusClient client);
}
