package com.gitlab.soshibby.modbus.broker.events;

import com.gitlab.soshibby.modbus.broker.client.ModBusClient;

import java.util.ArrayList;
import java.util.List;

public class MessageEventHandler implements EventHandler, MessageEventListener {

    private List<MessageEventListener> listeners = new ArrayList<>();

    public Class forClass() {
        return MessageEventListener.class;
    }

    @Override
    public void addListener(Object listener) {
        if (forClass().isInstance(listener)) {
            listeners.add((MessageEventListener) listener);
        } else {
            throw new RuntimeException("Invalid event listener, listener does not implement '" + forClass() + "' interface.");
        }
    }

    @Override
    public void onMessage(ModBusClient client, byte[] data) {
        listeners.forEach(listener -> listener.onMessage(client, data));
    }

}
