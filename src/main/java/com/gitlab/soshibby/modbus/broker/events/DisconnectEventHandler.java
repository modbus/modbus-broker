package com.gitlab.soshibby.modbus.broker.events;

import com.gitlab.soshibby.modbus.broker.client.ModBusClient;

import java.util.ArrayList;
import java.util.List;

public class DisconnectEventHandler implements EventHandler, DisconnectEventListener {

    private List<DisconnectEventListener> listeners = new ArrayList<>();

    public Class forClass() {
        return DisconnectEventListener.class;
    }

    @Override
    public void addListener(Object listener) {
        if (forClass().isInstance(listener)) {
            listeners.add((DisconnectEventListener) listener);
        } else {
            throw new RuntimeException("Invalid event listener, listener does not implement '" + forClass() + "' interface.");
        }
    }

    @Override
    public void onDisconnect(ModBusClient client) {
        listeners.forEach(listener -> listener.onDisconnect(client));
    }

}
