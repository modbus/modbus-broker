package com.gitlab.soshibby.modbus.broker.client;

import com.gitlab.soshibby.modbus.broker.events.EventDispatcher;
import com.gitlab.soshibby.modbus.broker.utils.Util;
import com.gitlab.soshibby.modbus.broker.events.DisconnectEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.UUID;

public class ModBusClient {

    private static final Logger log = LoggerFactory.getLogger(ModBusClient.class);
    private String id;
    private Socket socket;
    private EventDispatcher eventDispatcher;
    private boolean isConnected = true;
    private Thread listenThread;

    public ModBusClient(Socket socket, EventDispatcher eventDispatcher) {
        this.id = UUID.randomUUID().toString();
        this.socket = socket;
        this.eventDispatcher = eventDispatcher;
    }

    public String getId() {
        return id;
    }

    public void listen() {
        listenThread = new ClientListenThread(this, eventDispatcher);
        listenThread.setDaemon(true);
        listenThread.start();
    }

    public void send(byte[] data) throws IOException {
        log.debug("Sending data to '" + getId() + "', data: " + Util.bytesToHex(data));

        try {
            getSocket().getOutputStream().write(data);
            getSocket().getOutputStream().flush();
        } catch (IOException e) {
            log.error("Failed to write data to socket.", e);
            disconnect();
            throw e;
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void disconnect() {
        if (socket != null) {
            try {
                socket.close();
            } catch (Exception e) {
                log.error("Failed to close modbus client socket.", e);
            }

            listenThread.interrupt();
            DisconnectEventListener eventHandler = eventDispatcher.getEventHandlerFor(DisconnectEventListener.class);
            eventHandler.onDisconnect(this);
        }

        isConnected = false;
        socket = null;
    }

    protected InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    private Socket getSocket() {
        return socket;
    }

}

