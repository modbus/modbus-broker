package com.gitlab.soshibby.modbus.broker.events;

import com.gitlab.soshibby.modbus.broker.client.ModBusClient;

public interface MessageEventListener {
    void onMessage(ModBusClient client, byte[] data);
}
