package com.gitlab.soshibby.modbus.broker.server;

import com.gitlab.soshibby.modbus.broker.events.EventDispatcher;

import java.io.IOException;

public class ModBusServer extends Thread {
    private int port = 502;
    private ConnectionListenerThread listenerThread;
    private EventDispatcher eventDispatcher;

    public ModBusServer(EventDispatcher eventDispatcher) {
        this.eventDispatcher = eventDispatcher;
    }

    public ModBusServer(EventDispatcher eventDispatcher, int port) {
        this.eventDispatcher = eventDispatcher;
        this.port = port;
    }

    public void listen() throws IOException {
        listenerThread = new ConnectionListenerThread(eventDispatcher, port);
        listenerThread.start();
    }

    public void close() {
        listenerThread.interrupt();
    }

    protected void finalize() {
        listenerThread.interrupt();
    }

}
