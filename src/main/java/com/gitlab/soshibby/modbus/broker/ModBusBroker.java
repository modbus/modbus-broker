package com.gitlab.soshibby.modbus.broker;

import com.gitlab.soshibby.modbus.broker.events.DisconnectEventHandler;
import com.gitlab.soshibby.modbus.broker.events.MessageEventHandler;
import com.gitlab.soshibby.modbus.broker.events.MessageEventListener;
import com.gitlab.soshibby.modbus.broker.client.ModBusClient;
import com.gitlab.soshibby.modbus.broker.events.ConnectionEventHandler;
import com.gitlab.soshibby.modbus.broker.events.ConnectionEventListener;
import com.gitlab.soshibby.modbus.broker.events.DisconnectEventListener;
import com.gitlab.soshibby.modbus.broker.events.EventDispatcher;
import com.gitlab.soshibby.modbus.broker.server.ModBusServer;
import com.gitlab.soshibby.modbus.broker.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModBusBroker {

    private static final Logger log = LoggerFactory.getLogger(ModBusBroker.class);
    private static int MASTER_PORT = 502;
    private static int SLAVE_PORT = 503;
    private ModBusServer masterServer;
    private ModBusServer slaveServer;
    private List<ModBusClient> masters = new ArrayList<>();
    private List<ModBusClient> slaves = new ArrayList<>();

    public void start() {
        startMasterServer();
        startSlaveServer();
    }

    private void startMasterServer() {
        EventDispatcher masterEventDispatcher = new EventDispatcher();
        masterEventDispatcher.register(ConnectionEventListener.class, new ConnectionEventHandler());
        masterEventDispatcher.register(MessageEventListener.class, new MessageEventHandler());
        masterEventDispatcher.register(DisconnectEventListener.class, new DisconnectEventHandler());
        masterEventDispatcher.addListener(masterConnected);
        masterEventDispatcher.addListener(masterDisconnected);
        masterEventDispatcher.addListener(masterMessage);

        try {
            masterServer = new ModBusServer(masterEventDispatcher, MASTER_PORT);
            log.info("Start listening on master server.");
            masterServer.listen();
        } catch (IOException e) {
            log.error("Failed to start master server.", e);
            throw new RuntimeException("Failed to start master server.", e);
        }
    }

    private void startSlaveServer() {
        EventDispatcher slaveEventDispatcher = new EventDispatcher();
        slaveEventDispatcher.register(ConnectionEventListener.class, new ConnectionEventHandler());
        slaveEventDispatcher.register(MessageEventListener.class, new MessageEventHandler());
        slaveEventDispatcher.register(DisconnectEventListener.class, new DisconnectEventHandler());
        slaveEventDispatcher.addListener(slaveConnected);
        slaveEventDispatcher.addListener(slaveDisconnected);
        slaveEventDispatcher.addListener(slaveMessage);

        try {
            slaveServer = new ModBusServer(slaveEventDispatcher, SLAVE_PORT);
            log.info("Start listening on slave server.");
            slaveServer.listen();
        } catch (IOException e) {
            log.error("Failed to start slave server.", e);
            throw new RuntimeException("Failed to start slave server.", e);
        }
    }

    private DisconnectEventListener slaveDisconnected = client -> {
        log.info("Slave disconnected [" + client.getId() + "]");
        slaves.remove(client);
    };

    private ConnectionEventListener slaveConnected = client -> {
        log.info("Slave connected [" + client.getId() + "]");
        slaves.add(client);
    };

    private MessageEventListener slaveMessage = (client, data) -> {
        log.debug("Received slave message [" + client.getId() + "]: " + Util.bytesToHex(data));

        masters.forEach(master -> {
            try {
                master.send(data);
            } catch (IOException e) {
                log.error("Failed to send message to: " + master.getId(), e);
            }
        });
    };

    private DisconnectEventListener masterDisconnected = client -> {
        log.info("Master disconnected [" + client.getId() + "]");
        masters.remove(client);
    };

    private ConnectionEventListener masterConnected = client -> {
        log.info("Master connected [" + client.getId() + "]");
        masters.add(client);
    };

    private MessageEventListener masterMessage = (client, data) -> {
        log.debug("Received master message [" + client.getId() + "]: " + Util.bytesToHex(data));

        slaves.forEach(slave -> {
            try {
                slave.send(data);
            } catch (IOException e) {
                log.error("Failed to send message to: " + slave.getId(), e);
            }
        });
    };

}
