package com.gitlab.soshibby.modbus.broker.events;

import com.gitlab.soshibby.modbus.broker.client.ModBusClient;

import java.util.ArrayList;
import java.util.List;

public class ConnectionEventHandler implements EventHandler, ConnectionEventListener {

    private List<ConnectionEventListener> listeners = new ArrayList<>();

    public Class forClass() {
        return ConnectionEventListener.class;
    }

    @Override
    public void addListener(Object listener) {
        if (forClass().isInstance(listener)) {
            listeners.add((ConnectionEventListener) listener);
        } else {
            throw new RuntimeException("Invalid event listener, listener does not implement '" + forClass() + "' interface.");
        }
    }

    @Override
    public void onConnected(ModBusClient client) {
        listeners.forEach(listener -> listener.onConnected(client));
    }

}
