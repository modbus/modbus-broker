package com.gitlab.soshibby.modbus.broker.client;

import com.gitlab.soshibby.modbus.broker.events.EventDispatcher;
import com.gitlab.soshibby.modbus.broker.events.MessageEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Arrays;

class ClientListenThread extends Thread {
    private final Logger log = LoggerFactory.getLogger(ClientListenThread.class);
    private EventDispatcher eventDispatcher;
    private byte[] inBuffer = new byte[1024];
    private ModBusClient client;

    public ClientListenThread(ModBusClient client, EventDispatcher eventDispatcher) {
        this.client = client;
        this.eventDispatcher = eventDispatcher;
    }

    public void run() {
        try {
            InputStream inputStream = client.getInputStream();

            while (client.isConnected() && !Thread.interrupted()) {
                int numberOfBytes = inputStream.read(inBuffer);

                // -1 indicates that the client has closed the connection.
                if (numberOfBytes == -1) {
                    break;
                }

                if (numberOfBytes > 4) {
                    byte[] data = Arrays.copyOf(inBuffer, numberOfBytes);
                    onMessage(data);
                }

                Thread.sleep(5);
            }

            client.disconnect();
        } catch (Exception e) {
            log.error("Failed to receive client data.", e);
            client.disconnect();
        }
    }

    private void onMessage(byte[] data) {
        MessageEventListener eventHandler = eventDispatcher.getEventHandlerFor(MessageEventListener.class);
        eventHandler.onMessage(client, data);
    }

}
