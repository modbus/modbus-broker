package com.gitlab.soshibby.modbus.broker.events;

import java.util.HashMap;
import java.util.Map;

public class EventDispatcher {
    private Map<Class, EventHandler> eventHandlers = new HashMap<>();

    public void register(Class cls, EventHandler eventHandler) {
        eventHandlers.put(cls, eventHandler);
    }

    public void addListener(Object listener) {
        eventHandlers.forEach((cls, eventHandler) -> {
            if (cls.isInstance(listener)) {
                eventHandler.addListener(listener);
            }
        });
    }

    public <T> T getEventHandlerFor(Class<T> t) {
        if (eventHandlers.containsKey(t)) {
            return (T) eventHandlers.get(t);
        } else {
            throw new RuntimeException("No event handlers exists for the class '" + t.getSimpleName() + "'.");
        }
    }

}
