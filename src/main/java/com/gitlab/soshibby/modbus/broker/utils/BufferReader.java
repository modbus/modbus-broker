package com.gitlab.soshibby.modbus.broker.utils;

public class BufferReader {

    private short[] buffer;
    private int index;

    public BufferReader(short[] buffer) {
        this.buffer = buffer;
    }

    public int nextInt() {
        short[] byteData = new short[2];
        byteData[1] = buffer[index++];
        byteData[0] = buffer[index++];
        return byteArrayToInt(byteData);
    }

    public byte nextByte() {
        return (byte) buffer[index++];
    }

    public int byteArrayToInt(short[] byteArray) {
        int returnValue;
        returnValue = byteArray[0];
        returnValue = returnValue + 256 * byteArray[1];
        return returnValue;
    }
}
