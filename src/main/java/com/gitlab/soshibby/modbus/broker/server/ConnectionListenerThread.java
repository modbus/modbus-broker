package com.gitlab.soshibby.modbus.broker.server;

import com.gitlab.soshibby.modbus.broker.events.ConnectionEventListener;
import com.gitlab.soshibby.modbus.broker.events.EventDispatcher;
import com.gitlab.soshibby.modbus.broker.client.ModBusClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.net.Socket;

class ConnectionListenerThread extends Thread {

    private final Logger log = LoggerFactory.getLogger(ConnectionListenerThread.class);
    private EventDispatcher eventDispatcher;
    private int port;

    public ConnectionListenerThread(EventDispatcher eventDispatcher, int port) {
        this.eventDispatcher = eventDispatcher;
        this.port = port;
    }

    public void run() {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(port);
        } catch (Exception e) {
            log.error("Failed to create new server socket.", e);
            throw new RuntimeException(e);
        }

        while (!this.isInterrupted()) {
            try {
                acceptNewConnection(serverSocket);
            } catch (Exception e) {
                log.error("Failed to accept new connection.", e);
                throw e;
            }
        }

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (Exception e) {
                log.error("Failed to close server socket.", e);
                throw new RuntimeException(e);
            }
        }

        log.info("Closing down server socket on port {}.", port);
    }

    private void acceptNewConnection(ServerSocket serverSocket) {
        Socket socket;

        try {
            log.info("Listening for any new incoming connections...");
            socket = serverSocket.accept();
        } catch (Exception e) {
            log.error("Failed to accept new client connection.", e);
            return;
        }

        ModBusClient client = new ModBusClient(socket, eventDispatcher);
        ConnectionEventListener connectionEventListener = eventDispatcher.getEventHandlerFor(ConnectionEventListener.class);
        connectionEventListener.onConnected(client);
        client.listen();
    }

}
