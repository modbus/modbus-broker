package com.gitlab.soshibby.modbus.broker.events;

import com.gitlab.soshibby.modbus.broker.client.ModBusClient;

public interface DisconnectEventListener {
    void onDisconnect(ModBusClient client);
}
