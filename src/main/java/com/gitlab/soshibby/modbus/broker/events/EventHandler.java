package com.gitlab.soshibby.modbus.broker.events;

public interface EventHandler {
    void addListener(Object listener);
}
